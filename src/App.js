
import React, { Component } from "react"
import './App.css'; //



class Calculator extends React.Component {
  state = {
    operand1: ' ',
    operator: '',
    operand2: '',
    ultimate: '0',
    shown: false,

  }

  
 
  setOperand(value) {
    const {operand1, operand2, operator} = this.state;
   
    if (operator ==='') {
       this.setState({operand1: operand1 + value});    
    }
    else if (operator !=='') {
      this.setState({operand2: operand2 + value}); 
    } 
       
  }
    

  setOperator(value) {
    const {operator} = this.state;

    if (operator === '') {
      this.setState({operator: value}); 
    }  else if (operator !== '') {
      let result = this.getUltimate();
      this.setState({operand1: result, 
        ultimate: result,
        operand2: '', 
        operator: value});  
    }
  }

  getUltimate()  {
    let result = 0;
    const {operand1, operand2, operator} = this.state;
    const firstOperand = Number(operand1);
    const secondOperand = Number(operand2);
    
    switch (operator) {

      case '+':
      result  = firstOperand + secondOperand;
      break;

      case '-':
      result = firstOperand - secondOperand;
      break;

      case '*':
      result = firstOperand * secondOperand;
      break;
    
      case '/':
      result = firstOperand / secondOperand;
      break;
         
      case '**':
      result = firstOperand ** secondOperand;
      break;

      case '%':
      result =  operand1 / 100;
      break;
    }

    return result;
  }


  getResult() {
    this.setState({ ultimate: this.getUltimate(),
      shown: !this.state.shown, })
  }

  clearState() {
    this.setState({operand1: '', 
    operator: '', 
    operand2: '', 
    result: '',
    ultimate: '0',
     })
  }
  
  
  clearLastSubStr() {
    const {operand1, operand2, operator} = this.state;
    
    if (operator == '') {
      this.setState({operand1: operand1.slice(0, -1)});
    } else {
      this.setState({operand2: operand2.slice(0, -1)});
    }
  }


  render() {
    let shown = {
			display: this.state.shown ? 'none' : 'block'
		};
		
		let hidden = {
			display: this.state.shown ? 'block' : 'none'
    }
    
    const {ultimate, operand1, operand2, operator}  = this.state;

    return (
      <div className="calculator">
      <div className="display">{operand1}{operator}{operand2}</div>
      <div className="display">{ultimate}</div>
      <div className="operands">
        <button className="operandsbtn" style={ hidden } id="ac" onClick={()=>this.clearState()}>AC</button>
        <button className="operandsbtn" onClick={()=>this.setOperator("%")}>% </button>
        <button className="operandsbtn" onClick={()=>this.setOperator("/")} >÷</button>
        <button className="operandsbtn" onClick={()=>this.setOperator("*")}>×</button>
        <button className="operandsbtn" onClick={()=>this.setOperator("-")} >–</button>
        <button className="operandsbtn"onClick={()=>this.setOperator("+")} >+</button>
        <button className="operandsbtn" onClick={()=>this.setOperator("**")} >exp(x)</button>
        <button className="operandsbtn" style={ shown } onClick={()=>this.clearLastSubStr()} >C</button>
        
      </div>
  
      <div className="buttons">
  
        <button className="classic" onClick={()=>this.setOperand(9)} >9</button>
        <button className="classic" onClick={()=>this.setOperand(8)} >8</button>
        <button className="classic" onClick={()=>this.setOperand(7)}>7</button>
    
        <button className="classic" onClick={()=>this.setOperand(6)} >6</button>
        <button className="classic" onClick={()=>this.setOperand(5)} >5</button>
        <button className="classic" onClick={()=>this.setOperand(4)}>4</button>
    
    
        <button className="classic" onClick={()=>this.setOperand(3)} >3</button>
        <button className="classic" onClick={()=>this.setOperand(2)}>2</button>
        <button className="classic" onClick={()=>this.setOperand(1)}>1</button>
        <button className="classic zero" onClick={()=>this.setOperand(0)}>0</button>
        <button className="classic" onClick={()=>this.setOperand('.')}>.</button>
        <button className="equal" onClick={()=>this.getResult()}>=</button>
  
    </div>
  </div>
      )
  }
}




class App extends React.Component {

  render() {
    return ( <div><Calculator/></div>)
  }
}




export default App;
